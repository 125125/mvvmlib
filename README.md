# mvvmlib
MVVM Kotlin 基础依赖库
# 使用方法 [《Base基础库集成文档》](https://xiaoyuanrobot.yuque.com/docs/share/481d2276-baab-4904-b4b7-30a16eec279c?#)
## Step 1. Add the JitPack repository to your build file

```
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```
## Step 2. Add the dependency
 [![](https://jitpack.io/v/cjcj125125/mvvmlib.svg)](https://jitpack.io/#cjcj125125/mvvmlib)
请替换 Tag 为前面的版本号
```
dependencies {
	        implementation 'com.github.cjcj125125:mvvmlib:Tag'
	}
```
>> 提示：如果项目中有使用到kotlin room数据库，请添加如下依赖：
```
 kapt "androidx.room:room-compiler:2.4.1"
```
   
