package com.mvvm.lib.base

/**
 * @author : Aleyn
 * @date : 2022/07/31 : 20:28
 */
interface IViewModel {

    fun showLoading(text: String?="加载中...")

    fun dismissLoading()

}