package com.mvvm.lib.config

import com.mvvm.lib.network.ExceptionHandle

interface GlobalConfig {

   // fun viewModelFactory(): ViewModelProvider.Factory? = ViewModelFactory.getInstance()

    fun globalExceptionHandle(e: Throwable) = ExceptionHandle.handleException(e)

}