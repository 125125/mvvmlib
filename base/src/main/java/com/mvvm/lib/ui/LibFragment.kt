package com.mvvm.lib.ui

import android.os.Bundle
import com.mvvm.lib.base.BaseVMFragment
import com.mvvm.lib.base.NoViewModel
import com.mvvm.lib.databinding.FragmentLibBinding

class LibFragment : BaseVMFragment<NoViewModel, FragmentLibBinding>() {
    companion object {
        @JvmStatic
        fun newInstance() = LibFragment()
    }

    override var userDialog: Boolean = true

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        mBinding.fgShow.setOnClickListener {
            showLoading("请求网络中...")
            Thread {
                Thread.sleep(3000)
                mBinding.fgShow.post {
                    showLoading("Fg请求中，请稍等..")
                }
                Thread.sleep(3000)
                mBinding.fgShow.post {
                    dismissLoading()
                }
            }.start()
        }

    }


}