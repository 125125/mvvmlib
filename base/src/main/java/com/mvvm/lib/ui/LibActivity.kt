package com.mvvm.lib.ui

import android.os.Bundle
import androidx.fragment.app.commit
import com.mvvm.lib.R
import com.mvvm.lib.base.BaseVMActivity
import com.mvvm.lib.base.NoViewModel
import com.mvvm.lib.databinding.ActivityLibBinding

class LibActivity : BaseVMActivity<NoViewModel,ActivityLibBinding>() {


    override var userDialog: Boolean =true

    override fun createLoadView(maxWidth: Int) {
        super.createLoadView(600)
    }

    override fun initView(savedInstanceState: Bundle?) {
        supportFragmentManager.commit {
            add(R.id.container,LibFragment.newInstance())
        }
    }

    override fun initData() {
        mBinding.showDia.setOnClickListener {
            showLoading()
            Thread{
                Thread.sleep(3000)
                runOnUiThread{
                 showLoading("请求中，请稍等..")
                }
                Thread.sleep(3000)
                runOnUiThread{
                    dismissLoading()
                }
            }.start()
        }
    }

}