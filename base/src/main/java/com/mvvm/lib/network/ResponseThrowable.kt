package com.mvvm.lib.network

import com.mvvm.lib.base.IBaseResponse
import org.json.JSONObject
import retrofit2.HttpException

open class ResponseThrowable : Exception {
    var code: Int
    var errMsg: String

    constructor(  eh: HttpException ) : super(eh) {
       code = eh.code()
       val err= eh.response()?.errorBody()?.string()?:""
       errMsg = "${JSONObject(err)}"
    }
    constructor(error: ERROR, e: Throwable? = null) : super(e) {
        code = error.getKey()
        errMsg = error.getValue()
    }

    constructor(code: Int, msg: String, e: Throwable? = null) : super(e) {
        this.code = code
        this.errMsg = msg
    }

    constructor(base: IBaseResponse<*>, e: Throwable? = null) : super(e) {
        this.code = base.code()
        this.errMsg = base.msg()
    }
}

