package com.mvvm.lib.network

import com.blankj.utilcode.util.LogUtils
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *   @auther : 蔡杰
 *   time   : 2022/05/25
 */
class RetrofitClient {

    companion object {
        fun getInstance() = SingletonHolder.INSTANCE
        private fun getLoggingInterceptor(debug: Boolean): HttpLoggingInterceptor {
            val logger = HttpLoggingInterceptor()
            if (debug) {
                LogUtils.i("debug模式打印日志------")
                logger.level = HttpLoggingInterceptor.Level.BODY
            } else {
                logger.level = HttpLoggingInterceptor.Level.NONE
            }
            return logger
        }

        fun getOkHttpClient(debug: Boolean): OkHttpClient {
            return OkHttpClient.Builder()
                .hostnameVerifier { p0, p1 -> true }
                .connectTimeout(15L, TimeUnit.SECONDS)
                .callTimeout(45, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(46L, TimeUnit.SECONDS)
                //.addNetworkInterceptor(LoggingInterceptor())
                .connectionPool(ConnectionPool(20, 20, TimeUnit.SECONDS))
                .addInterceptor(getLoggingInterceptor(debug))
                .build()
        }

    }
    fun getOkHttpClient(debug: Boolean,timeOut:Long): OkHttpClient {
        return OkHttpClient.Builder()
            .hostnameVerifier { p0, p1 -> true }
            .connectTimeout(5L, TimeUnit.SECONDS)
            .callTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)
            //.addNetworkInterceptor(LoggingInterceptor())
            .connectionPool(ConnectionPool(20, 20, TimeUnit.SECONDS))
            .addInterceptor(getLoggingInterceptor(debug))
            .build()
    }

    private object SingletonHolder {
        val INSTANCE = RetrofitClient()
    }
    private   var retrofit: Retrofit?=null
    fun <T> create(service: Class<T>, baseUrl: String, debug: Boolean): T {
        retrofit ?:Retrofit.Builder()
            .client(getOkHttpClient(debug))
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build().also {
                retrofit=it
            }
        return retrofit?.create(service) ?: throw RuntimeException("Api service is null!")
    }

    fun <T> create(service: Class<T>, baseUrl: String,okHttpClient: OkHttpClient): T {
        retrofit ?:Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build().also {
                retrofit=it
            }
        return retrofit?.create(service) ?: throw RuntimeException("Api service is null!")
    }
}