package com.mvvm.lib.event

/**
 *
 * 消息对象
 */
class UserMessage @JvmOverloads constructor(
    var code: Int = 0,
    var msg: String = "",
    var arg1: Int = 0,
    var arg2: Int = 0,
    var obj: Any? = null
)